package main

import "fmt"

func main(){
	var n, oddSum, evenSum int

	fmt.Scan(&n)
	for i := 1; i <= n; i++ {
		if i%2 == 0 {
			evenSum+=i
		}
		if i%2 != 0 {
			oddSum+=i
		}

	}
	fmt.Println("Odd numbers sum:", oddSum)
	fmt.Println("Even numbers sum:", evenSum)
}

