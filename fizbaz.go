package main

import "fmt"

func main(){
	var i int
	fmt.Scan(&i)
	if i%5==0 && i%3==0 {
		fmt.Println("FizBaz")
	} else if i%3==0 {
		fmt.Println("Fiz")
	} else if i%5==0 {
		fmt.Println("Baz")
	} else {
		fmt.Println("not Fiz/Baz")
	}
	//fmt.Println()
}
