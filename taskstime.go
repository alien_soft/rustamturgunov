package main

import "fmt"
import "time"
//	 Создаём структуру "Задача"

type Task struct{
	assignee string
	title string
	time string
	done bool
	deadline bool
}

/* 	Создаём струткуру "Менеджер Задач", 
	которая хранит массив из "Задач"
*/
type TaskManager struct{
	tasks []Task
}

// Конструктор для "Менеджера задач"
func NewTaskManager() *TaskManager{
	tm := TaskManager{}
	tm.tasks = []Task{}
	return &tm
}

//Функция добавления новой задачи
func (tm *TaskManager) add(t Task){
	tm.tasks = append(tm.tasks, t)
}

//Функция назначения нового владельца задачи
func (tm *TaskManager) assign(name string, id int){
	tm.tasks[id].assignee = name
}

//Функция превращающая задачу в "сделанную"
func (tm *TaskManager) makeDone(id int){
	tm.tasks[id].done = true
}

//Функция проверки дедлайнов
func (tm *TaskManager) makeLate(){
	for i := 0; i < len(tm.tasks); i++{
		t, _ := time.Parse("2006-01-02", tm.tasks[i].time)
		fmt.Println(t)
		fmt.Println(tm.tasks[i].time)
		if !t.After(time.Now()) && !tm.tasks[i].done {
			tm.tasks[i].deadline = false
		} else {
			tm.tasks[i].deadline = true
		}
	}
}

//Функция распечатки одной Задачи
func (t *Task) list(){
	fmt.Println("Assignee:", t.assignee)
	fmt.Println("Title:", t.title)
	fmt.Println("Time:", t.time)
	fmt.Println("Done:", t.done)
	fmt.Println("Deadline:", t.deadline)
	fmt.Println()
}

//Фукнкия распечатки всех задач
func (tm *TaskManager) listAll(){
	for _, value := range tm.tasks{
		value.list()
	}
}

//Функция распечатки всех не сделанных задач
func (tm *TaskManager) listUnfinished(){
	for _, value := range tm.tasks{
		if !value.done {
			value.list()
		}
	}
}

func main(){
	tm := NewTaskManager()

	tm.add(Task{"Nurali", "Do contacts.go", "2019-09-20", false, false})

	fmt.Println("Added new task to Nurali")

	tm.tasks[0].list()

	fmt.Println()

	tm.assign("Rustam", 0)

	fmt.Println("Assigned first task to Rustam")

	tm.tasks[0].list()

	fmt.Println()

	tm.makeDone(0)

	fmt.Println("Made done first task")

	tm.tasks[0].list()

	fmt.Println()

	fmt.Println("Added new task to Daria")

	tm.add(Task{"Daria", "Hire new programmer", "2019-09-01", false, false})

	tm.add(Task{"Timur", "Finish backend", "2019-09-21", true, false})

	tm.listAll()

	fmt.Println()

	fmt.Println("Unfinished tasks")

	tm.listUnfinished()

	fmt.Println()

	fmt.Println("Made deadlines late for undone tasks")

	tm.makeLate()

	tm.listAll()

}
