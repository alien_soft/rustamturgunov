package tasks

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

var(
	tm *TaskManager
)

func TestTaskManagerAdd(t *testing.T){
	tm = NewTaskManager()

	tm.add(Task{"Rustam", "end the project", "2019-09-23", false})

	assert.NotEmpty(t, tm.tasks, "Task is not added")
}

func TestTaskManagerAssign(t *testing.T){
	tm.assign("Nurali", 0)

	assert.Equal(t, "Nurali", tm.tasks[0].assignee, "Task's assignee not updated")
}

func TestTaskManagerMakeDone(t *testing.T){
	tm.makeDone(0)

	assert.True(t, tm.tasks[0].done, "Task's field done is not put true")
}
