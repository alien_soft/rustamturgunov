package main

import "fmt"

func main() {
	var palin string
	fmt.Scan(&palin)
	fmt.Println(isPalindrom(palin))
}

func isPalindrom(line string) bool{
	q := len(line)-1
	for i := 0; i < len(line); i++{
		if(line[i]!=line[q]){
			return false
		}
		q--
	}
	return true
}
