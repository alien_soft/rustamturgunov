createdb fur_shop;

psql fur_shop

create table furniture_type
(
id serial primary key,
name varchar(40) not null
);

create table manufacturer
(
id serial primary key,
name varchar(40) not null,
country varchar(50) not null
);

create table consumer
(
id serial primary key,
name varchar(50) not null,
surname varchar(50),
address varchar(100),
phone varchar(25)
);

create table payment_form
(
id serial primary key,
type varchar(40) not null
);

create table furniture
(
id serial primary key,
name varchar(40) not null,
material varchar(40) not null,
manufacture_date date not null,
id_fur_type integer,
id_manufacturer integer,
foreign key (id_fur_type) references 
furniture_type (id) on update cascade on delete cascade,
foreign key (id_manufacturer) references 
manufacturer (id) on update cascade on delete cascade
);

create table sale
(
id serial primary key,
sale_date date not null,
sale_time time not null,
sale_sum integer not null,
id_fur integer,
id_consumer integer,
id_payment_form integer,
foreign key (id_fur) references 
furniture (id) on update cascade on delete cascade,
foreign key (id_consumer) references 
consumer (id) on update cascade on delete cascade,
foreign key (id_payment_form) references 
payment_form (id) on update cascade on delete cascade
);

insert into furniture_type(name) values('kitchen');
insert into furniture_type(name) values('lounge');
insert into furniture_type(name) values('soft');
insert into furniture_type(name) values('bedding');
insert into furniture_type(name) values('built-in');

insert into manufacturer(name, country) values('Hulsta', 'Germany');
insert into manufacturer(name, country) values('Bruehl', 'Germany');
insert into manufacturer(name, country) values('Amura', 'Italy');
insert into manufacturer(name, country) values('Alitavilla', 'Italy');
insert into manufacturer(name, country) values('Viva Deluxe', 'Uzbekistan');
insert into manufacturer(name, country) values('Katris', 'Uzbekistan');

insert into consumer(name, surname, address, phone) values('Farxod', 'Tojiyev', 'Olmazor district, Gulobod street, 32', '998977533443');
insert into consumer(name, surname, address, phone) values('Rustam', 'Turgunov', 'Mirobod district, Qarshi street, 96', '998906066006');
insert into consumer(name, surname, address, phone) values('Katya', 'Petrova', 'Mirobod district, Archazor street, 122', '998909003212');
insert into consumer(name, surname, address, phone) values('Konstantin', 'Ivanov', 'Yunusobod district, Alisher Navoi street, 21', '998944402390');
insert into consumer(name, surname, address, phone) values('Muxammadjon', 'Fayziyev', 'Yunusobod district, Ittifoq street, 98', '998936044343');
insert into consumer(name, surname, address, phone) values('Tsoy', 'Timur', 'Chilonzor district, Shahriston street, 89', '998977077070');
insert into consumer(name, surname, address, phone) values('Lee', 'Sun_Jo', 'Chilonzor district, Yashilzor street, 9', '998946454545');

insert into payment_form(type) values('uzcard');
insert into payment_form(type) values('visa');
insert into payment_form(type) values('cash');

insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('victory kitchen', 'LDSP', '2016-10-10', 1, 1);
insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('aloba sofa', 'plywood', '2017-01-30', 1, 1);
insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('midea chest of drawers', 'red oak', '2017-08-25', 2, 3);
insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('venice journal table', 'glass', '2017-06-23', 2, 4);
insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('straight sofa', 'nut-tree', '2016-12-23', 3, 5);
insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('rithm armchair', 'velveteen', '2017-09-10', 3, 5);
insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('vesta bed', 'sonoma', '2017-08-01', 4, 5);
insert into furniture(name, material, manufacture_date, id_fur_type, id_manufacturer) values('manhattan closet', 'oak', '2015-03-23', 4, 6);

insert into sale(sale_date, sale_time, sale_sum, id_fur, id_consumer, id_payment_form) values('2017-10-13', '08:31:45', 3000000, 1, 1, 1);
insert into sale(sale_date, sale_time, sale_sum, id_fur, id_consumer, id_payment_form) values('2017-10-13', '12:07:43', 2500000, 2, 2, 2);
insert into sale(sale_date, sale_time, sale_sum, id_fur, id_consumer, id_payment_form) values('2017-10-14', '16:09:00', 1200000, 3, 3, 3);
insert into sale(sale_date, sale_time, sale_sum, id_fur, id_consumer, id_payment_form) values('2017-10-14', '16:09:40', 5000000, 7, 3, 3);
insert into sale(sale_date, sale_time, sale_sum, id_fur, id_consumer, id_payment_form) values('2017-10-15', '13:57:00', 3300000, 6, 4, 3);
insert into sale(sale_date, sale_time, sale_sum, id_fur, id_consumer, id_payment_form) values('2017-10-16', '15:33:49', 650000, 4, 5, 3);
