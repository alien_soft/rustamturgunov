package contacts


import (
	"testing"
)


var (
	cm *ContactManager
)

func TestContactMangerAdd(t *testing.T) {
	cm = NewContactManager()

        cm.add(Contact{"Rustam", 23, "male", "+998908082596"})

        if (len(cm.contacts) == 0) {
		t.Error("Contact not added")
	}
}


func TestContactMangerUpdate(t *testing.T) {

        cm.update(0, Contact{"Nurali", 23, "male", "+998901221212"})

	if ( cm.contacts[0].name != "Nurali") {
		t.Error("Contact not updated")
	}

}

func TestContactMangerGetAll(t *testing.T) {
        if (len(cm.getAll()) == 0) {
		t.Error("Cannot get all contacts")
	}
}

func TestContactMangerDel(t *testing.T) {
	cm.del(0)

        if (len(cm.getAll()) != 0) {
		t.Error("Contact not deleted")
	}
}
