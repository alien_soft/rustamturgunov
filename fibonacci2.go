package main


import "fmt"

func main(){
	var n int
	fmt.Scan(&n)

	if n==1 || n==2{
		fmt.Println(1)
		return
	}

	var fib []int = make([]int, n)
	fib[0] = 1
	fib[1] = 1
	
	for i := 2; i < n; i++{
		fib[i]=fib[i-1]+fib[i-2]
	}
	
	fmt.Println(fib[n-1])
}
