package main

import "fmt"

//Структура "Контакт" хранит один контакт

type Contact struct{
	name string
	age int
	gender string
	number string
}

//Структура "Менеджер Контактов" хранит массив структур "Контакт"
type ContactManager struct {
	contacts []Contact
}

//Конструктор для структуры Менджер Контаков
func NewContactManager() *ContactManager {

	cm := ContactManager{}
	cm.contacts = []Contact{}

	return &cm
}

//Функция распечатки одного контакта
func (c Contact) print(){
	fmt.Println(c.name)
	fmt.Println(c.age)
	fmt.Println(c.gender)
	fmt.Println(c.number)
}

//Функция добавления нового контакта в массив
func (c *ContactManager) add(ct Contact) {
	c.contacts = append(c.contacts, ct)
}

//Функция изменения определённого контакта
func (c *ContactManager) update(i int, ct Contact) {
	c.contacts[i] = ct
}

//Функция удаления контакта по айди
func (c *ContactManager) del(i int) {
	c.contacts = append(c.contacts[:i], c.contacts[i+1:]...)
}

//Функция распечатки всех контактов
func (c *ContactManager) printAll(){
	for _, value := range c.contacts{
		value.print()
	}
}




func main() {

	cm := NewContactManager()

	fmt.Println("Добавляем нового юзера")

	cm.add(Contact{"Rustam", 23, "male", "+998908082596"})

	cm.update(0, Contact{"Nurali", 23, "male", "+998901221212"})

	cm.printAll()

	cm.del(0)

	cm.printAll()

	cm.add(Contact{"Aziza", 23, "female", "+998902343223"})

	cm.printAll()

	cm.contacts[0].print()
}
