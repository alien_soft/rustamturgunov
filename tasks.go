package main

import "fmt"

//	 Создаём структуру "Задача"

type Task struct{
	assignee string
	title string
	time string
	done bool
}

/* 	Создаём струткуру "Менеджер Задач", 
	которая хранит массив из "Задач"
*/
type TaskManager struct{
	tasks []Task
}

// Конструктор для "Менеджера задач"
func NewTaskManager() *TaskManager{
	tm := TaskManager{}
	tm.tasks = []Task{}

	return &tm
}

//Функция добавления новой задачи
func (tm *TaskManager) add(t Task){
	tm.tasks = append(tm.tasks, t)
}

//Функция назначения нового владельца задачи
func (tm *TaskManager) assign(name string, id int){
	tm.tasks[id].assignee = name
}

//Функция превращающая задачу в "сделанную"
func (tm *TaskManager) makeDone(id int){
	tm.tasks[id].done = true
}

//Функция распечатки одной Задачи
func (t *Task) list(){
	fmt.Println(t.assignee)
	fmt.Println(t.title)
	fmt.Println(t.time)
	fmt.Println("done:", t.done)
}

//Фукнкия распечатки всех задач
func (tm *TaskManager) listAll(){
	for _, value := range tm.tasks{
		value.list()
	}
}

//Функция распечатки всех не сделанных задач
func (tm *TaskManager) listUnfinished(){
	for _, value := range tm.tasks{
		if(!value.done){
			value.list()
		}
	}
}

func main(){
	tm := NewTaskManager()

	tm.add(Task{"Nurali", "Do contacts.go", "20.09.2019", false})
	
	tm.tasks[0].list()

	tm.assign("Rustam", 0)

	tm.tasks[0].list()
	
	tm.makeDone(0)

	tm.tasks[0].list()

	tm.add(Task{"Daria", "Hire new programmer", "25.09.2019", false})

	tm.listAll()

	fmt.Println("Unfinished tasks")

	tm.listUnfinished()

}
