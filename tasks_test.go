package tasks

import (
	"testing"
)

var(
	tm *TaskManager
)

func TestTaskManagerAdd(t *testing.T){
	tm = NewTaskManager()

	tm.add(Task{"Rustam", "end the project", "2019-09-23", false})

	if(len(tm.tasks)<1){
		t.Error("Task not added")
	}
}

func TestTaskManagerAssign(t *testing.T){
	tm.assign("Nurali", 0)

	if(tm.tasks[0].assignee != "Nurali"){
		t.Error("Task isn't assigned")
	}
}

func TestTaskManagerMakeDone(t *testing.T){
	tm.makeDone(0)

	if(tm.tasks[0].done != true){
		t.Error("Task isn't changed to \"done\"")
	}
