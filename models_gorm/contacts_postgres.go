package models

import (
	_ "database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type ContactManager struct {
	db *gorm.DB
}


func NewContactManager() (ContactManagerI, error) {
	cm := ContactManager{}
	var err error
	connStr := "user=postgres dbname=contacts password=123 host=localhost sslmode=disable"
	cm.db, err = gorm.Open("postgres", connStr)
	cm.db.AutoMigrate(&Contact{})
	if err != nil {
		return nil, err
	}
	return &cm, nil
}

//Adding new contact to database
func (c *ContactManager) Add(ct Contact) error {
	if result := c.db.Create(&ct); result.Error != nil{
		return result.Error
	}
	return nil
}

//Updating particular contact
func (c *ContactManager) Update(i int, ct Contact) error {
	c.db.LogMode(true)
	if result := c.db.Model(&ct).Where("id = ?", i).Updates(&ct); result.Error !=nil{
		return result.Error
	}
	return nil
}

//Функция удаления контакта по айди
func (c *ContactManager) Del(i int) error {
	if result := c.db.Where("id = ?", i).Delete(&Contact{}); result.Error != nil{
		return result.Error
	}
	return nil
}

//Функция распечатки всех контактов
func (c *ContactManager) GetAll() ([]Contact, error) {
	var contacts []Contact
	result := c.db.Find(&contacts)
	if result.Error !=nil{
		return nil, result.Error
	}
	return contacts, nil
}

func (c *ContactManager)ListAll() error{
	var contacts []Contact
	result := c.db.Find(&contacts)
	if result.Error !=nil{
		return nil
	}
	fmt.Println(contacts)
	return nil
}
