package main

import "github.com/jinzhu/gorm"

type Contact struct{
	gorm.Model
	Name	string 	`db:"not null"`
	Age		int		`db:"not null"`
	Gender	string	`db:"not null"`
	Number	string	`db:"not null"`
}

type ContactManagerI interface {
	Add (ct Contact, reply *Contact) error
	Update (ct Contact, reply *Contact) error
	Del (i int, reply *Contact) error
	GetAll (ct Contact, reply *[]Contact) error
}
