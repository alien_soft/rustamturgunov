package models

import "github.com/jinzhu/gorm"

type Contact struct{
	gorm.Model
	Name	string 	`db:"not null "`
	Age		int		`db:"not null"`
	Gender	string	`db:"not null"`
	Number	string	`db:"not null"`
}

type ContactManagerI interface {
	Add (ct Contact) error
	Update (i int, ct Contact) error
	Del (i int) error
	GetAll () ([]Contact, error)
	ListAll () error
}
