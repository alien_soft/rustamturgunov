package models


import (
	"github.com/jmoiron/sqlx"
	"testing"
)

var (
	cm ContactManagerI
	db *sqlx.DB
)

func TestContactManger(t *testing.T) {
	var err error
	cm, err = NewContactManager()
	if(err != nil){
		t.Error("Can't connect to database")
	}
}

func TestContactManagerAdd(t *testing.T){
	err := cm.Add(Contact{Name:"Nurali", Age: 23, Gender: "male", Number: "2323"})
        if (err != nil) {
		t.Error("Contact not added")
	}
}


func TestContactMangerUpdate(t *testing.T) {

	err := cm.Update(3, Contact{Name: "Nurali", Age: 23, Gender: "male", Number: "+998901221212"})

        if (err != nil) {
		t.Error("Contact not updated")
	}

}

func TestContactMangerGetAll(t *testing.T) {
	_, err := cm.GetAll()
        if (err != nil) {
		t.Error("Cannot get all contacts: ", err)
	}
}

func TestContactMangerDel(t *testing.T) {
	err := cm.Del(2)
	if (err != nil) {
		t.Error("Contact not deleted", err)
	}
}
